package gyurix.configfile;

public interface PostLoadable {
    void postLoad();
}
